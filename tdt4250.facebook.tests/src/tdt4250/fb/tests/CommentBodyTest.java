/**
 */
package tdt4250.fb.tests;

import junit.textui.TestRunner;

import tdt4250.fb.CommentBody;
import tdt4250.fb.FbFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Comment Body</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CommentBodyTest extends AbstractContentBodyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CommentBodyTest.class);
	}

	/**
	 * Constructs a new Comment Body test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommentBodyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Comment Body test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CommentBody getFixture() {
		return (CommentBody)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FbFactory.eINSTANCE.createCommentBody());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CommentBodyTest
