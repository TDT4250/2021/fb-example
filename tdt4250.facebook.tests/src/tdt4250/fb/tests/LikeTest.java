/**
 */
package tdt4250.fb.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.fb.FbFactory;
import tdt4250.fb.Like;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Like</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LikeTest extends TestCase {

	/**
	 * The fixture for this Like test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Like fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LikeTest.class);
	}

	/**
	 * Constructs a new Like test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LikeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Like test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Like fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Like test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Like getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FbFactory.eINSTANCE.createLike());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LikeTest
