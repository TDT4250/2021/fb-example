/**
 */
package tdt4250.fb.tests;

import junit.framework.TestCase;

import tdt4250.fb.AbstractContentBody;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Content Body</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AbstractContentBodyTest extends TestCase {

	/**
	 * The fixture for this Abstract Content Body test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractContentBody fixture = null;

	/**
	 * Constructs a new Abstract Content Body test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractContentBodyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Abstract Content Body test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(AbstractContentBody fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Abstract Content Body test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractContentBody getFixture() {
		return fixture;
	}

} //AbstractContentBodyTest
