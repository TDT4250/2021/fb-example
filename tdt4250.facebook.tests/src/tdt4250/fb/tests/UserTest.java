/**
 */
package tdt4250.fb.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.fb.FbFactory;
import tdt4250.fb.User;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link tdt4250.fb.User#getName() <em>Name</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class UserTest extends TestCase {

	/**
	 * The fixture for this User test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected User fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UserTest.class);
	}

	/**
	 * Constructs a new User test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this User test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(User fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this User test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected User getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FbFactory.eINSTANCE.createUser());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.fb.User#getName() <em>Name</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.User#getName()
	 * @generated NOT
	 */
	public void testGetName() {
		User user = getFixture();
		assertEquals(null, user.getName());
		user.setGivenName("Hallvard");
		assertEquals("Hallvard", user.getName());
		user.setFamilyName("Trætteberg");
		assertEquals("Hallvard Trætteberg", user.getName());
		user.setGivenName(null);
		assertEquals("Trætteberg", user.getName());
	}

} //UserTest
