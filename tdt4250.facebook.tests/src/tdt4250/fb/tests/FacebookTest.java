/**
 */
package tdt4250.fb.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.fb.Facebook;
import tdt4250.fb.FbFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Facebook</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FacebookTest extends TestCase {

	/**
	 * The fixture for this Facebook test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Facebook fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FacebookTest.class);
	}

	/**
	 * Constructs a new Facebook test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FacebookTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Facebook test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Facebook fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Facebook test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Facebook getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FbFactory.eINSTANCE.createFacebook());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FacebookTest
