/**
 */
package tdt4250.fb.tests;

import junit.textui.TestRunner;

import tdt4250.fb.FbFactory;
import tdt4250.fb.PostBody;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Post Body</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PostBodyTest extends AbstractContentBodyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PostBodyTest.class);
	}

	/**
	 * Constructs a new Post Body test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PostBodyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Post Body test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PostBody getFixture() {
		return (PostBody)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FbFactory.eINSTANCE.createPostBody());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //PostBodyTest
