/**
 */
package tdt4250.fb.tests;

import junit.textui.TestRunner;

import tdt4250.fb.FbFactory;
import tdt4250.fb.Share;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Share</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ShareTest extends PostTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ShareTest.class);
	}

	/**
	 * Constructs a new Share test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShareTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Share test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Share getFixture() {
		return (Share)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FbFactory.eINSTANCE.createShare());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ShareTest
