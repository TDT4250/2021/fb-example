package tdt4250.fb.tests;

import java.util.List;

import org.eclipse.acceleo.query.delegates.AQLValidationDelegate;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EValidator.ValidationDelegate;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;

import junit.framework.TestCase;
import tdt4250.fb.Facebook;
import tdt4250.fb.FbFactory;
import tdt4250.fb.FbPackage;
import tdt4250.fb.Like;
import tdt4250.fb.Post;
import tdt4250.fb.Share;
import tdt4250.fb.User;
import tdt4250.fb.util.FbResourceFactoryImpl;

public class FbValidatorTest extends TestCase {

//	private Resource testInstance;
//	private Diagnostic diagnostics;
	
	protected Resource loadFbResource(String name) {
		// A container of Resource, whether loaded explicitly or on-demand (automatically)
		ResourceSet resSet = new ResourceSetImpl();
		// relate the FbPackage identifier used in XMI files to the FbPackage instance (EPackage meta-object) 
		resSet.getPackageRegistry().put(FbPackage.eNS_URI, FbPackage.eINSTANCE);
		// map the 'fb' file extension to our custom Resource.Factory implementation
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("fb", new FbResourceFactoryImpl());
		
		return resSet.getResource(URI.createURI(FbValidatorTest.class.getResource(name + ".fb").toString()), true);
	}
	
	protected void setUp() throws Exception {
		// register AQL (an OCL implementation) constraint support, so we can test OCL constraints, too
		ValidationDelegate.Registry.INSTANCE.put("http://www.eclipse.org/acceleo/query/1.0", new AQLValidationDelegate());
	}
	
	public void testConstraint_userNotAuthor1() {
		// create post with a like from other user
		User user1 = FbFactory.eINSTANCE.createUser();
		User user2 = FbFactory.eINSTANCE.createUser();
		Post post = FbFactory.eINSTANCE.createPost();
		post.setAuthor(user1);
		Like like = FbFactory.eINSTANCE.createLike();
		post.getLikes().add(like);
		like.setUser(user2);
		// run validation and check result
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(like);
		assertTrue(diagnostics.getSeverity() == Diagnostic.OK);
		// make like user and post author the same
		like.setUser(user1);
		// re-run validation and check result
		diagnostics = Diagnostician.INSTANCE.validate(like);
		assertTrue(diagnostics.getSeverity() == Diagnostic.ERROR);
	}
	
	// test constraints with instances loaded from pre-made resource
	public void testConstraint_userNotAuthor2() {
		// load test resource
		Resource resource = loadFbResource("testConstraint_userNotAuthor2");
		// navigate to likes
		List<Like> likes = ((Facebook) resource.getContents().get(0)).getUsers().get(1).getPosts().get(0).getLikes();
		// run validation one first like and check result
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(likes.get(0));
		assertTrue(diagnostics.getSeverity() == Diagnostic.OK);
		// run validation one second like and check result
		diagnostics = Diagnostician.INSTANCE.validate(likes.get(1));
		assertTrue(diagnostics.getSeverity() == Diagnostic.ERROR);
	}
	
	public void testConstraint_shareAuthorNotPostAuthor1() {
		// create post and share with different authors
		User user1 = FbFactory.eINSTANCE.createUser();
		User user2 = FbFactory.eINSTANCE.createUser();
		Post post = FbFactory.eINSTANCE.createPost();
		post.setAuthor(user1);
		Share share = FbFactory.eINSTANCE.createShare();
		share.setOriginal(post);
		share.setAuthor(user2);
		// run validation and check result
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(share);
		assertTrue(diagnostics.getSeverity() == Diagnostic.OK);
		// make authors the same
		share.setAuthor(user1);
		// re-run validation and check result
		diagnostics = Diagnostician.INSTANCE.validate(share);
		assertTrue(diagnostics.getSeverity() == Diagnostic.ERROR);
	}
}
