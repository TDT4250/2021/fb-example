/**
 */
package tdt4250.fb;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Share</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.fb.Share#getOriginal <em>Original</em>}</li>
 * </ul>
 *
 * @see tdt4250.fb.FbPackage#getShare()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='shareAuthorNotPostAuthor'"
 * @generated
 */
public interface Share extends Post {
	/**
	 * Returns the value of the '<em><b>Original</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Original</em>' reference.
	 * @see #setOriginal(Post)
	 * @see tdt4250.fb.FbPackage#getShare_Original()
	 * @model
	 * @generated
	 */
	Post getOriginal();

	/**
	 * Sets the value of the '{@link tdt4250.fb.Share#getOriginal <em>Original</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Original</em>' reference.
	 * @see #getOriginal()
	 * @generated
	 */
	void setOriginal(Post value);

} // Share
