/**
 */
package tdt4250.fb;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facebook</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.fb.Facebook#getUsers <em>Users</em>}</li>
 * </ul>
 *
 * @see tdt4250.fb.FbPackage#getFacebook()
 * @model
 * @generated
 */
public interface Facebook extends EObject {
	/**
	 * Returns the value of the '<em><b>Users</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.fb.User}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Users</em>' containment reference list.
	 * @see tdt4250.fb.FbPackage#getFacebook_Users()
	 * @model containment="true"
	 * @generated
	 */
	EList<User> getUsers();

} // Facebook
