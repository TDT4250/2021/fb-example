/**
 */
package tdt4250.fb.util;

import java.time.LocalDate;
import java.time.LocalDateTime;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import tdt4250.fb.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see tdt4250.fb.FbPackage
 * @generated
 */
public class FbValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final FbValidator INSTANCE = new FbValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "tdt4250.fb";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FbValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return FbPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case FbPackage.FACEBOOK:
				return validateFacebook((Facebook)value, diagnostics, context);
			case FbPackage.USER:
				return validateUser((User)value, diagnostics, context);
			case FbPackage.ABSTRACT_CONTENT:
				return validateAbstractContent((AbstractContent)value, diagnostics, context);
			case FbPackage.POST:
				return validatePost((Post)value, diagnostics, context);
			case FbPackage.COMMENT:
				return validateComment((Comment)value, diagnostics, context);
			case FbPackage.ABSTRACT_CONTENT_BODY:
				return validateAbstractContentBody((AbstractContentBody)value, diagnostics, context);
			case FbPackage.POST_BODY:
				return validatePostBody((PostBody)value, diagnostics, context);
			case FbPackage.COMMENT_BODY:
				return validateCommentBody((CommentBody)value, diagnostics, context);
			case FbPackage.LIKE:
				return validateLike((Like)value, diagnostics, context);
			case FbPackage.SHARE:
				return validateShare((Share)value, diagnostics, context);
			case FbPackage.LIKE_KIND:
				return validateLikeKind((LikeKind)value, diagnostics, context);
			case FbPackage.ELOCAL_DATE_TIME:
				return validateELocalDateTime((LocalDateTime)value, diagnostics, context);
			case FbPackage.ELOCAL_DATE:
				return validateELocalDate((LocalDate)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFacebook(Facebook facebook, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(facebook, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUser(User user, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(user, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePost(Post post, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(post, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePostBody(PostBody postBody, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(postBody, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLike(Like like, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(like, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(like, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(like, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(like, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(like, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(like, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(like, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(like, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(like, diagnostics, context);
		if (result || diagnostics != null) result &= validateLike_userNotAuthor(like, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the userNotAuthor constraint of '<em>Like</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String LIKE__USER_NOT_AUTHOR__EEXPRESSION = "self.user <> self.content.author";

	/**
	 * Validates the userNotAuthor constraint of '<em>Like</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLike_userNotAuthor(Like like, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(FbPackage.Literals.LIKE,
				 like,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "userNotAuthor",
				 LIKE__USER_NOT_AUTHOR__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComment(Comment comment, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(comment, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractContentBody(AbstractContentBody abstractContentBody, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(abstractContentBody, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommentBody(CommentBody commentBody, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(commentBody, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShare(Share share, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(share, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(share, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(share, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(share, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(share, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(share, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(share, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(share, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(share, diagnostics, context);
		if (result || diagnostics != null) result &= validateShare_shareAuthorNotPostAuthor(share, diagnostics, context);
		return result;
	}

	/**
	 * Validates the shareAuthorNotPostAuthor constraint of '<em>Share</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateShare_shareAuthorNotPostAuthor(Share share, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (share.getAuthor() == share.getOriginal().getAuthor()) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "shareAuthorNotPostAuthor", getObjectLabel(share, context) },
						 new Object[] { share },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractContent(AbstractContent abstractContent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(abstractContent, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLikeKind(LikeKind likeKind, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateELocalDate(LocalDate eLocalDate, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateELocalDateTime(LocalDateTime eLocalDateTime, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //FbValidator
