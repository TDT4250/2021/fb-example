/**
 */
package tdt4250.fb;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250.fb.FbFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0'"
 * @generated
 */
public interface FbPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "fb";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/tdt4250.facebook/model/fb.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fb";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FbPackage eINSTANCE = tdt4250.fb.impl.FbPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.FacebookImpl <em>Facebook</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.FacebookImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getFacebook()
	 * @generated
	 */
	int FACEBOOK = 0;

	/**
	 * The feature id for the '<em><b>Users</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACEBOOK__USERS = 0;

	/**
	 * The number of structural features of the '<em>Facebook</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACEBOOK_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Facebook</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACEBOOK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.UserImpl <em>User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.UserImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getUser()
	 * @generated
	 */
	int USER = 1;

	/**
	 * The feature id for the '<em><b>Given Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__GIVEN_NAME = 0;

	/**
	 * The feature id for the '<em><b>Family Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__FAMILY_NAME = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__NAME = 2;

	/**
	 * The feature id for the '<em><b>Birthday</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__BIRTHDAY = 3;

	/**
	 * The feature id for the '<em><b>Posts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__POSTS = 4;

	/**
	 * The number of structural features of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.AbstractContentImpl <em>Abstract Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.AbstractContentImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getAbstractContent()
	 * @generated
	 */
	int ABSTRACT_CONTENT = 2;

	/**
	 * The feature id for the '<em><b>Author</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTENT__AUTHOR = 0;

	/**
	 * The feature id for the '<em><b>Likes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTENT__LIKES = 1;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTENT__TIMESTAMP = 2;

	/**
	 * The number of structural features of the '<em>Abstract Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Abstract Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.PostImpl <em>Post</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.PostImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getPost()
	 * @generated
	 */
	int POST = 3;

	/**
	 * The feature id for the '<em><b>Author</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST__AUTHOR = ABSTRACT_CONTENT__AUTHOR;

	/**
	 * The feature id for the '<em><b>Likes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST__LIKES = ABSTRACT_CONTENT__LIKES;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST__TIMESTAMP = ABSTRACT_CONTENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST__BODY = ABSTRACT_CONTENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST__COMMENTS = ABSTRACT_CONTENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Post</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST_FEATURE_COUNT = ABSTRACT_CONTENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Post</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST_OPERATION_COUNT = ABSTRACT_CONTENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.PostBodyImpl <em>Post Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.PostBodyImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getPostBody()
	 * @generated
	 */
	int POST_BODY = 6;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.LikeImpl <em>Like</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.LikeImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getLike()
	 * @generated
	 */
	int LIKE = 8;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.CommentImpl <em>Comment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.CommentImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getComment()
	 * @generated
	 */
	int COMMENT = 4;

	/**
	 * The feature id for the '<em><b>Author</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__AUTHOR = ABSTRACT_CONTENT__AUTHOR;

	/**
	 * The feature id for the '<em><b>Likes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LIKES = ABSTRACT_CONTENT__LIKES;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__TIMESTAMP = ABSTRACT_CONTENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__BODY = ABSTRACT_CONTENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Replies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__REPLIES = ABSTRACT_CONTENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_FEATURE_COUNT = ABSTRACT_CONTENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_OPERATION_COUNT = ABSTRACT_CONTENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.AbstractContentBodyImpl <em>Abstract Content Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.AbstractContentBodyImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getAbstractContentBody()
	 * @generated
	 */
	int ABSTRACT_CONTENT_BODY = 5;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTENT_BODY__CONTENTS = 0;

	/**
	 * The number of structural features of the '<em>Abstract Content Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTENT_BODY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Abstract Content Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_CONTENT_BODY_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST_BODY__CONTENTS = ABSTRACT_CONTENT_BODY__CONTENTS;

	/**
	 * The number of structural features of the '<em>Post Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST_BODY_FEATURE_COUNT = ABSTRACT_CONTENT_BODY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Post Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POST_BODY_OPERATION_COUNT = ABSTRACT_CONTENT_BODY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.CommentBodyImpl <em>Comment Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.CommentBodyImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getCommentBody()
	 * @generated
	 */
	int COMMENT_BODY = 7;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_BODY__CONTENTS = ABSTRACT_CONTENT_BODY__CONTENTS;

	/**
	 * The number of structural features of the '<em>Comment Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_BODY_FEATURE_COUNT = ABSTRACT_CONTENT_BODY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Comment Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_BODY_OPERATION_COUNT = ABSTRACT_CONTENT_BODY_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIKE__USER = 0;

	/**
	 * The feature id for the '<em><b>Like</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIKE__LIKE = 1;

	/**
	 * The feature id for the '<em><b>Content</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIKE__CONTENT = 2;

	/**
	 * The number of structural features of the '<em>Like</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIKE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Like</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIKE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.fb.impl.ShareImpl <em>Share</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.impl.ShareImpl
	 * @see tdt4250.fb.impl.FbPackageImpl#getShare()
	 * @generated
	 */
	int SHARE = 9;

	/**
	 * The feature id for the '<em><b>Author</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARE__AUTHOR = POST__AUTHOR;

	/**
	 * The feature id for the '<em><b>Likes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARE__LIKES = POST__LIKES;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARE__TIMESTAMP = POST__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARE__BODY = POST__BODY;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARE__COMMENTS = POST__COMMENTS;

	/**
	 * The feature id for the '<em><b>Original</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARE__ORIGINAL = POST_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Share</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARE_FEATURE_COUNT = POST_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Share</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARE_OPERATION_COUNT = POST_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250.fb.LikeKind <em>Like Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.fb.LikeKind
	 * @see tdt4250.fb.impl.FbPackageImpl#getLikeKind()
	 * @generated
	 */
	int LIKE_KIND = 10;

	/**
	 * The meta object id for the '<em>ELocal Date</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.time.LocalDate
	 * @see tdt4250.fb.impl.FbPackageImpl#getELocalDate()
	 * @generated
	 */
	int ELOCAL_DATE = 12;

	/**
	 * The meta object id for the '<em>ELocal Date Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.time.LocalDateTime
	 * @see tdt4250.fb.impl.FbPackageImpl#getELocalDateTime()
	 * @generated
	 */
	int ELOCAL_DATE_TIME = 11;


	/**
	 * Returns the meta object for class '{@link tdt4250.fb.Facebook <em>Facebook</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Facebook</em>'.
	 * @see tdt4250.fb.Facebook
	 * @generated
	 */
	EClass getFacebook();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.fb.Facebook#getUsers <em>Users</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Users</em>'.
	 * @see tdt4250.fb.Facebook#getUsers()
	 * @see #getFacebook()
	 * @generated
	 */
	EReference getFacebook_Users();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.User <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User</em>'.
	 * @see tdt4250.fb.User
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.fb.User#getGivenName <em>Given Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Given Name</em>'.
	 * @see tdt4250.fb.User#getGivenName()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_GivenName();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.fb.User#getFamilyName <em>Family Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Family Name</em>'.
	 * @see tdt4250.fb.User#getFamilyName()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_FamilyName();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.fb.User#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.fb.User#getName()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.fb.User#getBirthday <em>Birthday</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Birthday</em>'.
	 * @see tdt4250.fb.User#getBirthday()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Birthday();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.fb.User#getPosts <em>Posts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Posts</em>'.
	 * @see tdt4250.fb.User#getPosts()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Posts();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.Post <em>Post</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Post</em>'.
	 * @see tdt4250.fb.Post
	 * @generated
	 */
	EClass getPost();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.fb.Post#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see tdt4250.fb.Post#getBody()
	 * @see #getPost()
	 * @generated
	 */
	EReference getPost_Body();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.fb.Post#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see tdt4250.fb.Post#getComments()
	 * @see #getPost()
	 * @generated
	 */
	EReference getPost_Comments();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.PostBody <em>Post Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Post Body</em>'.
	 * @see tdt4250.fb.PostBody
	 * @generated
	 */
	EClass getPostBody();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.Like <em>Like</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Like</em>'.
	 * @see tdt4250.fb.Like
	 * @generated
	 */
	EClass getLike();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.fb.Like#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User</em>'.
	 * @see tdt4250.fb.Like#getUser()
	 * @see #getLike()
	 * @generated
	 */
	EReference getLike_User();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.fb.Like#getLike <em>Like</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Like</em>'.
	 * @see tdt4250.fb.Like#getLike()
	 * @see #getLike()
	 * @generated
	 */
	EAttribute getLike_Like();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.fb.Like#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Content</em>'.
	 * @see tdt4250.fb.Like#getContent()
	 * @see #getLike()
	 * @generated
	 */
	EReference getLike_Content();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.Comment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comment</em>'.
	 * @see tdt4250.fb.Comment
	 * @generated
	 */
	EClass getComment();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.fb.Comment#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see tdt4250.fb.Comment#getBody()
	 * @see #getComment()
	 * @generated
	 */
	EReference getComment_Body();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.fb.Comment#getReplies <em>Replies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Replies</em>'.
	 * @see tdt4250.fb.Comment#getReplies()
	 * @see #getComment()
	 * @generated
	 */
	EReference getComment_Replies();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.AbstractContentBody <em>Abstract Content Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Content Body</em>'.
	 * @see tdt4250.fb.AbstractContentBody
	 * @generated
	 */
	EClass getAbstractContentBody();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.fb.AbstractContentBody#getContents <em>Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Contents</em>'.
	 * @see tdt4250.fb.AbstractContentBody#getContents()
	 * @see #getAbstractContentBody()
	 * @generated
	 */
	EAttribute getAbstractContentBody_Contents();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.CommentBody <em>Comment Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comment Body</em>'.
	 * @see tdt4250.fb.CommentBody
	 * @generated
	 */
	EClass getCommentBody();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.Share <em>Share</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Share</em>'.
	 * @see tdt4250.fb.Share
	 * @generated
	 */
	EClass getShare();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.fb.Share#getOriginal <em>Original</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Original</em>'.
	 * @see tdt4250.fb.Share#getOriginal()
	 * @see #getShare()
	 * @generated
	 */
	EReference getShare_Original();

	/**
	 * Returns the meta object for class '{@link tdt4250.fb.AbstractContent <em>Abstract Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Content</em>'.
	 * @see tdt4250.fb.AbstractContent
	 * @generated
	 */
	EClass getAbstractContent();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.fb.AbstractContent#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Author</em>'.
	 * @see tdt4250.fb.AbstractContent#getAuthor()
	 * @see #getAbstractContent()
	 * @generated
	 */
	EReference getAbstractContent_Author();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.fb.AbstractContent#getLikes <em>Likes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Likes</em>'.
	 * @see tdt4250.fb.AbstractContent#getLikes()
	 * @see #getAbstractContent()
	 * @generated
	 */
	EReference getAbstractContent_Likes();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.fb.AbstractContent#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see tdt4250.fb.AbstractContent#getTimestamp()
	 * @see #getAbstractContent()
	 * @generated
	 */
	EAttribute getAbstractContent_Timestamp();

	/**
	 * Returns the meta object for enum '{@link tdt4250.fb.LikeKind <em>Like Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Like Kind</em>'.
	 * @see tdt4250.fb.LikeKind
	 * @generated
	 */
	EEnum getLikeKind();

	/**
	 * Returns the meta object for data type '{@link java.time.LocalDate <em>ELocal Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ELocal Date</em>'.
	 * @see java.time.LocalDate
	 * @model instanceClass="java.time.LocalDate"
	 * @generated
	 */
	EDataType getELocalDate();

	/**
	 * Returns the meta object for data type '{@link java.time.LocalDateTime <em>ELocal Date Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>ELocal Date Time</em>'.
	 * @see java.time.LocalDateTime
	 * @model instanceClass="java.time.LocalDateTime"
	 * @generated
	 */
	EDataType getELocalDateTime();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FbFactory getFbFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.FacebookImpl <em>Facebook</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.FacebookImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getFacebook()
		 * @generated
		 */
		EClass FACEBOOK = eINSTANCE.getFacebook();

		/**
		 * The meta object literal for the '<em><b>Users</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FACEBOOK__USERS = eINSTANCE.getFacebook_Users();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.UserImpl <em>User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.UserImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getUser()
		 * @generated
		 */
		EClass USER = eINSTANCE.getUser();

		/**
		 * The meta object literal for the '<em><b>Given Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__GIVEN_NAME = eINSTANCE.getUser_GivenName();

		/**
		 * The meta object literal for the '<em><b>Family Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__FAMILY_NAME = eINSTANCE.getUser_FamilyName();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__NAME = eINSTANCE.getUser_Name();

		/**
		 * The meta object literal for the '<em><b>Birthday</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__BIRTHDAY = eINSTANCE.getUser_Birthday();

		/**
		 * The meta object literal for the '<em><b>Posts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__POSTS = eINSTANCE.getUser_Posts();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.PostImpl <em>Post</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.PostImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getPost()
		 * @generated
		 */
		EClass POST = eINSTANCE.getPost();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POST__BODY = eINSTANCE.getPost_Body();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POST__COMMENTS = eINSTANCE.getPost_Comments();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.PostBodyImpl <em>Post Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.PostBodyImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getPostBody()
		 * @generated
		 */
		EClass POST_BODY = eINSTANCE.getPostBody();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.LikeImpl <em>Like</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.LikeImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getLike()
		 * @generated
		 */
		EClass LIKE = eINSTANCE.getLike();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIKE__USER = eINSTANCE.getLike_User();

		/**
		 * The meta object literal for the '<em><b>Like</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIKE__LIKE = eINSTANCE.getLike_Like();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIKE__CONTENT = eINSTANCE.getLike_Content();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.CommentImpl <em>Comment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.CommentImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getComment()
		 * @generated
		 */
		EClass COMMENT = eINSTANCE.getComment();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMENT__BODY = eINSTANCE.getComment_Body();

		/**
		 * The meta object literal for the '<em><b>Replies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMENT__REPLIES = eINSTANCE.getComment_Replies();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.AbstractContentBodyImpl <em>Abstract Content Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.AbstractContentBodyImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getAbstractContentBody()
		 * @generated
		 */
		EClass ABSTRACT_CONTENT_BODY = eINSTANCE.getAbstractContentBody();

		/**
		 * The meta object literal for the '<em><b>Contents</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_CONTENT_BODY__CONTENTS = eINSTANCE.getAbstractContentBody_Contents();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.CommentBodyImpl <em>Comment Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.CommentBodyImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getCommentBody()
		 * @generated
		 */
		EClass COMMENT_BODY = eINSTANCE.getCommentBody();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.ShareImpl <em>Share</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.ShareImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getShare()
		 * @generated
		 */
		EClass SHARE = eINSTANCE.getShare();

		/**
		 * The meta object literal for the '<em><b>Original</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHARE__ORIGINAL = eINSTANCE.getShare_Original();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.impl.AbstractContentImpl <em>Abstract Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.impl.AbstractContentImpl
		 * @see tdt4250.fb.impl.FbPackageImpl#getAbstractContent()
		 * @generated
		 */
		EClass ABSTRACT_CONTENT = eINSTANCE.getAbstractContent();

		/**
		 * The meta object literal for the '<em><b>Author</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_CONTENT__AUTHOR = eINSTANCE.getAbstractContent_Author();

		/**
		 * The meta object literal for the '<em><b>Likes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_CONTENT__LIKES = eINSTANCE.getAbstractContent_Likes();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_CONTENT__TIMESTAMP = eINSTANCE.getAbstractContent_Timestamp();

		/**
		 * The meta object literal for the '{@link tdt4250.fb.LikeKind <em>Like Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.fb.LikeKind
		 * @see tdt4250.fb.impl.FbPackageImpl#getLikeKind()
		 * @generated
		 */
		EEnum LIKE_KIND = eINSTANCE.getLikeKind();

		/**
		 * The meta object literal for the '<em>ELocal Date</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.time.LocalDate
		 * @see tdt4250.fb.impl.FbPackageImpl#getELocalDate()
		 * @generated
		 */
		EDataType ELOCAL_DATE = eINSTANCE.getELocalDate();

		/**
		 * The meta object literal for the '<em>ELocal Date Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.time.LocalDateTime
		 * @see tdt4250.fb.impl.FbPackageImpl#getELocalDateTime()
		 * @generated
		 */
		EDataType ELOCAL_DATE_TIME = eINSTANCE.getELocalDateTime();

	}

} //FbPackage
