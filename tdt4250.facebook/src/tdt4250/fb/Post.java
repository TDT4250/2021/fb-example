/**
 */
package tdt4250.fb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Post</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.fb.Post#getBody <em>Body</em>}</li>
 *   <li>{@link tdt4250.fb.Post#getComments <em>Comments</em>}</li>
 * </ul>
 *
 * @see tdt4250.fb.FbPackage#getPost()
 * @model
 * @generated
 */
public interface Post extends AbstractContent {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(PostBody)
	 * @see tdt4250.fb.FbPackage#getPost_Body()
	 * @model containment="true"
	 * @generated
	 */
	PostBody getBody();

	/**
	 * Sets the value of the '{@link tdt4250.fb.Post#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(PostBody value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.fb.Comment}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see tdt4250.fb.FbPackage#getPost_Comments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Comment> getComments();

} // Post
