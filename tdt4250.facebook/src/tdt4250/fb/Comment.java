/**
 */
package tdt4250.fb;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.fb.Comment#getBody <em>Body</em>}</li>
 *   <li>{@link tdt4250.fb.Comment#getReplies <em>Replies</em>}</li>
 * </ul>
 *
 * @see tdt4250.fb.FbPackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends AbstractContent {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(CommentBody)
	 * @see tdt4250.fb.FbPackage#getComment_Body()
	 * @model containment="true"
	 * @generated
	 */
	CommentBody getBody();

	/**
	 * Sets the value of the '{@link tdt4250.fb.Comment#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(CommentBody value);

	/**
	 * Returns the value of the '<em><b>Replies</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.fb.Comment}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Replies</em>' containment reference list.
	 * @see tdt4250.fb.FbPackage#getComment_Replies()
	 * @model containment="true"
	 * @generated
	 */
	EList<Comment> getReplies();

} // Comment
