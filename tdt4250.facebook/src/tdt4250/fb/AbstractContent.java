/**
 */
package tdt4250.fb;

import java.time.LocalDateTime;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Content</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.fb.AbstractContent#getAuthor <em>Author</em>}</li>
 *   <li>{@link tdt4250.fb.AbstractContent#getLikes <em>Likes</em>}</li>
 *   <li>{@link tdt4250.fb.AbstractContent#getTimestamp <em>Timestamp</em>}</li>
 * </ul>
 *
 * @see tdt4250.fb.FbPackage#getAbstractContent()
 * @model abstract="true"
 * @generated
 */
public interface AbstractContent extends EObject {
	/**
	 * Returns the value of the '<em><b>Author</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Author</em>' reference.
	 * @see #setAuthor(User)
	 * @see tdt4250.fb.FbPackage#getAbstractContent_Author()
	 * @model required="true"
	 * @generated
	 */
	User getAuthor();

	/**
	 * Sets the value of the '{@link tdt4250.fb.AbstractContent#getAuthor <em>Author</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Author</em>' reference.
	 * @see #getAuthor()
	 * @generated
	 */
	void setAuthor(User value);

	/**
	 * Returns the value of the '<em><b>Likes</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.fb.Like}.
	 * It is bidirectional and its opposite is '{@link tdt4250.fb.Like#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Likes</em>' containment reference list.
	 * @see tdt4250.fb.FbPackage#getAbstractContent_Likes()
	 * @see tdt4250.fb.Like#getContent
	 * @model opposite="content" containment="true"
	 * @generated
	 */
	EList<Like> getLikes();

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(LocalDateTime)
	 * @see tdt4250.fb.FbPackage#getAbstractContent_Timestamp()
	 * @model dataType="tdt4250.fb.ELocalDateTime"
	 * @generated
	 */
	LocalDateTime getTimestamp();

	/**
	 * Sets the value of the '{@link tdt4250.fb.AbstractContent#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(LocalDateTime value);

} // AbstractContent
