/**
 */
package tdt4250.fb;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment Body</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tdt4250.fb.FbPackage#getCommentBody()
 * @model
 * @generated
 */
public interface CommentBody extends AbstractContentBody {
} // CommentBody
