/**
 */
package tdt4250.fb;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Post Body</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tdt4250.fb.FbPackage#getPostBody()
 * @model
 * @generated
 */
public interface PostBody extends AbstractContentBody {
} // PostBody
