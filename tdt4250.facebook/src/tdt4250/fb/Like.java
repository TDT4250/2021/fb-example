/**
 */
package tdt4250.fb;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Like</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.fb.Like#getUser <em>User</em>}</li>
 *   <li>{@link tdt4250.fb.Like#getLike <em>Like</em>}</li>
 *   <li>{@link tdt4250.fb.Like#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @see tdt4250.fb.FbPackage#getLike()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='userNotAuthor'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 userNotAuthor='self.user &lt;&gt; self.content.author'"
 * @generated
 */
public interface Like extends EObject {
	/**
	 * Returns the value of the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' reference.
	 * @see #setUser(User)
	 * @see tdt4250.fb.FbPackage#getLike_User()
	 * @model
	 * @generated
	 */
	User getUser();

	/**
	 * Sets the value of the '{@link tdt4250.fb.Like#getUser <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' reference.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(User value);

	/**
	 * Returns the value of the '<em><b>Like</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.fb.LikeKind}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Like</em>' attribute.
	 * @see tdt4250.fb.LikeKind
	 * @see #setLike(LikeKind)
	 * @see tdt4250.fb.FbPackage#getLike_Like()
	 * @model
	 * @generated
	 */
	LikeKind getLike();

	/**
	 * Sets the value of the '{@link tdt4250.fb.Like#getLike <em>Like</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Like</em>' attribute.
	 * @see tdt4250.fb.LikeKind
	 * @see #getLike()
	 * @generated
	 */
	void setLike(LikeKind value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.fb.AbstractContent#getLikes <em>Likes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' container reference.
	 * @see #setContent(AbstractContent)
	 * @see tdt4250.fb.FbPackage#getLike_Content()
	 * @see tdt4250.fb.AbstractContent#getLikes
	 * @model opposite="likes" transient="false"
	 * @generated
	 */
	AbstractContent getContent();

	/**
	 * Sets the value of the '{@link tdt4250.fb.Like#getContent <em>Content</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' container reference.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(AbstractContent value);

} // Like
