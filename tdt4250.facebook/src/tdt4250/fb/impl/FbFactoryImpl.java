/**
 */
package tdt4250.fb.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import tdt4250.fb.Comment;
import tdt4250.fb.CommentBody;
import tdt4250.fb.Facebook;
import tdt4250.fb.FbFactory;
import tdt4250.fb.FbPackage;
import tdt4250.fb.Like;
import tdt4250.fb.LikeKind;
import tdt4250.fb.Post;
import tdt4250.fb.PostBody;
import tdt4250.fb.Share;
import tdt4250.fb.User;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FbFactoryImpl extends EFactoryImpl implements FbFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FbFactory init() {
		try {
			FbFactory theFbFactory = (FbFactory)EPackage.Registry.INSTANCE.getEFactory(FbPackage.eNS_URI);
			if (theFbFactory != null) {
				return theFbFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FbFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FbFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FbPackage.FACEBOOK: return createFacebook();
			case FbPackage.USER: return createUser();
			case FbPackage.POST: return createPost();
			case FbPackage.COMMENT: return createComment();
			case FbPackage.POST_BODY: return createPostBody();
			case FbPackage.COMMENT_BODY: return createCommentBody();
			case FbPackage.LIKE: return createLike();
			case FbPackage.SHARE: return createShare();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case FbPackage.LIKE_KIND:
				return createLikeKindFromString(eDataType, initialValue);
			case FbPackage.ELOCAL_DATE_TIME:
				return createELocalDateTimeFromString(eDataType, initialValue);
			case FbPackage.ELOCAL_DATE:
				return createELocalDateFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case FbPackage.LIKE_KIND:
				return convertLikeKindToString(eDataType, instanceValue);
			case FbPackage.ELOCAL_DATE_TIME:
				return convertELocalDateTimeToString(eDataType, instanceValue);
			case FbPackage.ELOCAL_DATE:
				return convertELocalDateToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Facebook createFacebook() {
		FacebookImpl facebook = new FacebookImpl();
		return facebook;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public User createUser() {
		UserImpl user = new UserImpl();
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Post createPost() {
		PostImpl post = new PostImpl();
		return post;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PostBody createPostBody() {
		PostBodyImpl postBody = new PostBodyImpl();
		return postBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Like createLike() {
		LikeImpl like = new LikeImpl();
		return like;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comment createComment() {
		CommentImpl comment = new CommentImpl();
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommentBody createCommentBody() {
		CommentBodyImpl commentBody = new CommentBodyImpl();
		return commentBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Share createShare() {
		ShareImpl share = new ShareImpl();
		return share;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LikeKind createLikeKindFromString(EDataType eDataType, String initialValue) {
		LikeKind result = LikeKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLikeKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public LocalDate createELocalDateFromString(EDataType eDataType, String initialValue) {
		return initialValue == null || initialValue.isBlank() ? null : LocalDate.parse(initialValue, DateTimeFormatter.ISO_LOCAL_DATE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertELocalDateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? "" : DateTimeFormatter.ISO_LOCAL_DATE.format((LocalDateTime) instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public LocalDateTime createELocalDateTimeFromString(EDataType eDataType, String initialValue) {
		return initialValue == null || initialValue.isBlank() ? null : LocalDateTime.parse(initialValue, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertELocalDateTimeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? "" : DateTimeFormatter.ISO_LOCAL_DATE_TIME.format((LocalDateTime) instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FbPackage getFbPackage() {
		return (FbPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FbPackage getPackage() {
		return FbPackage.eINSTANCE;
	}

} //FbFactoryImpl
