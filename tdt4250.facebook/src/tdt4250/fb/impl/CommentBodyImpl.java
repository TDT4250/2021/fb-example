/**
 */
package tdt4250.fb.impl;

import org.eclipse.emf.ecore.EClass;

import tdt4250.fb.CommentBody;
import tdt4250.fb.FbPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comment Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CommentBodyImpl extends AbstractContentBodyImpl implements CommentBody {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommentBodyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FbPackage.Literals.COMMENT_BODY;
	}

} //CommentBodyImpl
