/**
 */
package tdt4250.fb.impl;

import org.eclipse.emf.ecore.EClass;

import tdt4250.fb.FbPackage;
import tdt4250.fb.PostBody;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Post Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PostBodyImpl extends AbstractContentBodyImpl implements PostBody {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PostBodyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FbPackage.Literals.POST_BODY;
	}

} //PostBodyImpl
