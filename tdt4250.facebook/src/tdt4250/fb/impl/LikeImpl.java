/**
 */
package tdt4250.fb.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;
import tdt4250.fb.AbstractContent;
import tdt4250.fb.FbPackage;
import tdt4250.fb.Like;
import tdt4250.fb.LikeKind;
import tdt4250.fb.User;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Like</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.fb.impl.LikeImpl#getUser <em>User</em>}</li>
 *   <li>{@link tdt4250.fb.impl.LikeImpl#getLike <em>Like</em>}</li>
 *   <li>{@link tdt4250.fb.impl.LikeImpl#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LikeImpl extends MinimalEObjectImpl.Container implements Like {
	/**
	 * The cached value of the '{@link #getUser() <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUser()
	 * @generated
	 * @ordered
	 */
	protected User user;

	/**
	 * The default value of the '{@link #getLike() <em>Like</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLike()
	 * @generated
	 * @ordered
	 */
	protected static final LikeKind LIKE_EDEFAULT = LikeKind.LIKE;

	/**
	 * The cached value of the '{@link #getLike() <em>Like</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLike()
	 * @generated
	 * @ordered
	 */
	protected LikeKind like = LIKE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LikeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FbPackage.Literals.LIKE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public User getUser() {
		if (user != null && user.eIsProxy()) {
			InternalEObject oldUser = (InternalEObject)user;
			user = (User)eResolveProxy(oldUser);
			if (user != oldUser) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FbPackage.LIKE__USER, oldUser, user));
			}
		}
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public User basicGetUser() {
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUser(User newUser) {
		User oldUser = user;
		user = newUser;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FbPackage.LIKE__USER, oldUser, user));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LikeKind getLike() {
		return like;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLike(LikeKind newLike) {
		LikeKind oldLike = like;
		like = newLike == null ? LIKE_EDEFAULT : newLike;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FbPackage.LIKE__LIKE, oldLike, like));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractContent getContent() {
		if (eContainerFeatureID() != FbPackage.LIKE__CONTENT) return null;
		return (AbstractContent)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContent(AbstractContent newContent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContent, FbPackage.LIKE__CONTENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(AbstractContent newContent) {
		if (newContent != eInternalContainer() || (eContainerFeatureID() != FbPackage.LIKE__CONTENT && newContent != null)) {
			if (EcoreUtil.isAncestor(this, newContent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContent != null)
				msgs = ((InternalEObject)newContent).eInverseAdd(this, FbPackage.ABSTRACT_CONTENT__LIKES, AbstractContent.class, msgs);
			msgs = basicSetContent(newContent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FbPackage.LIKE__CONTENT, newContent, newContent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FbPackage.LIKE__CONTENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContent((AbstractContent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FbPackage.LIKE__CONTENT:
				return basicSetContent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case FbPackage.LIKE__CONTENT:
				return eInternalContainer().eInverseRemove(this, FbPackage.ABSTRACT_CONTENT__LIKES, AbstractContent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FbPackage.LIKE__USER:
				if (resolve) return getUser();
				return basicGetUser();
			case FbPackage.LIKE__LIKE:
				return getLike();
			case FbPackage.LIKE__CONTENT:
				return getContent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FbPackage.LIKE__USER:
				setUser((User)newValue);
				return;
			case FbPackage.LIKE__LIKE:
				setLike((LikeKind)newValue);
				return;
			case FbPackage.LIKE__CONTENT:
				setContent((AbstractContent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FbPackage.LIKE__USER:
				setUser((User)null);
				return;
			case FbPackage.LIKE__LIKE:
				setLike(LIKE_EDEFAULT);
				return;
			case FbPackage.LIKE__CONTENT:
				setContent((AbstractContent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FbPackage.LIKE__USER:
				return user != null;
			case FbPackage.LIKE__LIKE:
				return like != LIKE_EDEFAULT;
			case FbPackage.LIKE__CONTENT:
				return getContent() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (like: ");
		result.append(like);
		result.append(')');
		return result.toString();
	}

} //LikeImpl
