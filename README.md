# fb-example

Facebook model example

## Introduction

This example shows the use of basic ecore features. So far it covers users and posts/shares, with comments/replies and likes.

## Use of ecore feature

### EClass

Most concepts are modelled as EClasses, e.g. User, Post, Comment etc. This is similar to how it would have been modelled using UML. The Like EClass would correspond to an Association class in UML, at it conceptually is an association with attached data (the kind of like).

The main use of inheritance is for gathering features (attributes and references) that are common to several classes in abstract superclasses. Thus, AbstractContent is the abstract superclass of Post and Comment, since both these have an author and can be liked. Similarly, the body of posts and comments have AbstractContentBody as common abstract superclass.

### EDataType

EDataTypes is Ecore's way of supporting the use of ordinary Java classes for value-like objects. Here we define EDateTypes for time-related classes. Ecore already includes EDate, but since Java now have new classes for dates, time and time of day, we define our own for ELocalDate (used for birthday) and ELocalDateTime (used for timestamp). The naming convention seems to be to prefix the java class with an E.

For each EDataType we must ensure that the logic in the generated factory implementation for converting to and from Strings (for serialization or data input) is correct. In our case we changed the `convert` and `create` methods to use standard DataTimeFormatter instances that can convert back and forth. And make sure to change `@generated` to `@generated NOT`, to avoid overwriting the changed code when regenerating the code.

### EEnum

EEnums are similar to Java enums and represents a (small) set of fixed symbolic values, that may have some natural order (or at least an associated numeral). Here we use it for kinds of likes. My naming convention is to use "Kind" as suffix.

### EStructuralFeatures

EAttributes and EReferences are features (subclasses of EStructuralFeature) with types that are EDataTypes and EClasses, respectively. The most important feature of features besides the name and type is the multiplicity. By setting the upper bound to a value >1 makes it many-valued, and -1 makes it unbounded.

### Constraints

Constraints are declared using EAnnotations (which are attached to any model element and carry custom key/value pairs that are processed by certain tools) and provide predicates that check for the validity of whole instances (or structures of them) or feature values. The predicate logic can be implemented in two main ways, either by generating code and filling in stubs for the declared constraints, or by providing the implementation in another EAnnotation with the source code in a interpreted language like OCL.

This model has two constraints implemented by either means. The `userNotAuthor` constraint is attached to the Like EClass and checks that a user doesn't like his/her own content. It is implemented in OCL attached to the same EClass. The `shareAuthorNotPostAuthor` constraint checks that a user doesn't share his/her own Posts. It is implemented in Java in the generated FbValidator subclass of EObjectValidator (implements constraints that are common to all instances).

### Derived features

Feature values can be derived (or computed) from other features, with custom logic. Usually such a feature is not changeable (no setter), transient (not serialized) and volatile (no field for storing it), instead the value is computed on demand, either by manually written code in the generated getter or by an interpreted expression. A changeable derived feature also needs setter logic.

Our model derives the name of a User from the givenName and familyName using java code in UserImpl.getName.

## Genmodel and EMF features

### Generated tests

Code that is completely generated from the model, need not be tested, but features like derived features and constraints implemented using interpreted or compiled java code needs tests.

The generated **tests** project contains test classes for each domain class with stub methods that **fail()** for cases EMF knows needs tests. E.g. our derived **User.name** feature has a test covering relevant cases.

### Specialized editor for fb instances

To create a custom editor for instances of our model, we set the **File Extension** for our model to `fb` (the default) and set the **Resource Type** to **XMI**. By generating **edit** and **editor** projects, we get a pretty generic editor specialized to and registered for our model and its `fb` file extension. Using the generated wizard, we can create and edit instances that can be validated using both interpreted code, e.g. OCL, and compiled java code. Also derived features implemented in java will be available.
